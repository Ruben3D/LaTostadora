// Angular
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// Services
import { CatalogService } from '../../services/catalog.service';

// Models
import { Product } from '../../models/product';
import { Included } from '../../models/included';

@Component({
  selector: 'product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})

export class ProductItemComponent implements OnInit {

  // Class Properties
  arrGender: Included[] = [];
  arrType: Included[] = [];
  genderToShow: string;
  urlImage: string;
  count: number = 0;
  itemSelected: any;
  addButtonShow: boolean = false;

  // Inputs Component
  @Input()
  product: Product;

  @Input()
  included: Included[];
  
  // This input (gender) call a function when change his value. Uses getter and setter!
  @Input()
  set gender(g) {
    this.genderToShow = g;
    this.addButtonShow = false;
    this.filterGender();
  }
  get gender() {
    return this.genderToShow;
  }
  
  // Outputs Component
  @Output() 
  countEvent = new EventEmitter<number>();


  /**
   * @param catalog The injected CatalogService.
   */
  constructor(private catalog: CatalogService) { }

  /**
   * Lifecycle Hook - Initialize the component after Angular first displays
   */
  ngOnInit() {
    // Call to function to filter by gender products
    this.filterGender();
  }

  /**
   * This function choose a selected item and show the button to add to cart and set path of urlImage
   * @param item Receive a item
   */
  chooseItem(item) {
    this.urlImage = item.attributes.image;
    this.itemSelected = item;
    this.addButtonShow = true;
    // console.log(item);
  }

  /**
   * Function to add to cart a item selected
   */
  addToCart() {
    this.count++;
    this.countEvent.emit(this.itemSelected);
  }

  


  /////////////////////
  /* PRIVATE METHODS */
  /////////////////////
  
  /**
   * Function to filter by gender when starts app for first time
   */
  private filterGender() {
    if (this.gender == 'hombre') {
      this.arrGender = this.included.filter(
        element => {
          if (element.id.startsWith('1-1') || element.id.startsWith('2-1')) {
            return element;
          }
        }
      );
    } else {
      this.arrGender = this.included.filter(
        element => {
          if (element.id.startsWith('1-2') ||  element.id.startsWith('2-2')) {
            return element;
          }
        }
      );
    }
    // Call to function to filter by Type of tshirt
    this.filterByType();
  }

  /**
   * Alert!!! --> Uses the array with name (arrType) created before in function filterGender()
   * Function to filter by type of product
   */
  private filterByType() {
    if (this.product.id === '1') {
      this.arrType = this.arrGender.filter(
        element => element.id.startsWith('1')
      );
    } else {
      this.arrType = this.arrGender.filter(
        element => element.id.startsWith('2')
      );
    }

    // Default background images of cards
    if (this.product.id === '1' && this.gender === 'hombre') {
      this.urlImage = 'assets/images/catalog/1/1-1-1-man_white_tshirt.jpg';
    } else if (this.product.id === '2' && this.gender === 'hombre'){
      this.urlImage = 'assets/images/catalog/2/2-1-1-man_white_tshirt.jpg';
    } else if (this.product.id === '1' && this.gender === 'mujer') {
      this.urlImage = 'assets/images/catalog/1/1-2-1-woman_black_tshirt.jpg';
    } else if (this.product.id === '2' && this.gender === 'mujer') {
      this.urlImage = 'assets/images/catalog/2/2-2-1-woman_black_tshirt.jpg';
    }
  }

}
