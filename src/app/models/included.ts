

export interface Included {

  attributes: Attributes;
  id: string;
  relationships?: { colors: { data: { id: string; type: string }[] } };
  type: string;

}

interface Attributes {

  default_variant_id?: string;
  style?: string;
  color?: string;
  currency?: string;
  image?: string;
  price?: number;


}

