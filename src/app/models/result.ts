import { Product } from './product';
import { Included } from './included';


export interface Result {

  data: Product[];
  included: Included[];

}
