

export interface Product {

  attributes: { default_variant_id: string; title: string; };
  id: string;
  relationships: { styles: { data: { id: string; type: string; }[]; }, type: string; };

}
