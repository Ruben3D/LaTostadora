// Angular
import { Component } from '@angular/core';

//Services
import { CatalogService } from './services/catalog.service';

// Models
import { Product } from './models/product';
import { Included } from './models/included';

// Components
import { ProductItemComponent } from "./components/product-item/product-item.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  // Class Properties
  products: Product[] = [];
  included: Included[] = [];
  cartItems: any[string] = [];
  count: number;
  gender: string = 'hombre';

  /**
   * @param catalog The injected CatalogService.
   */
  constructor(private catalog: CatalogService) {

    /**
     * Subscribe to Observable into service to get all products with http-get method
     */
    catalog.getAllProducts()
      .subscribe(data => {
        this.products = data.data;
        this.included = data.included;
      });
    }

  /**
   * Function to push item into array of cart
   * @param $event Receive the event
   */
  receiveCount($event) {
    let item = $event

    // Push in array with custom name for man
    if (item.id === "1-1-1") {
      this.cartItems.push("Camiseta Hombre Blanca - CCCP");
    } else if (item.id === "1-1-2") {
      this.cartItems.push("Camiseta Hombre Gris - CCCP");
    } else if (item.id === "2-1-1") {
      this.cartItems.push("Camiseta Hombre Blanca - Vietnam War");
    } else if (item.id === "2-1-2") {
      this.cartItems.push("Camiseta Hombre Gris - Vietnam War");
    }

    // Push in array with custom name for woman
    if (item.id === "1-2-1") {
      this.cartItems.push("Camiseta Mujer Negra - CCCP");
    } else if (item.id === "1-2-2") {
      this.cartItems.push("Camiseta Mujer Blanca - CCCP");
    } else if (item.id === "2-2-1") {
      this.cartItems.push("Camiseta Mujer Negra - Vietnam War");
    } else if (item.id === "2-2-2") {
      this.cartItems.push("Camiseta Mujer Blanca - Vietnam War");
    }
   
    // Size of Cart with lenght of items
    this.count = this.cartItems.length;
  }

  /**
   * Function to delete item from cart
   * @param item Receive the item
   */
  deleteItemFromCart(item) {
    this.cartItems.splice(item, 1);
    this.count = this.cartItems.length;
  }

  /**
   * Function to select and switch property of gender
   * @param gender Receive gender
   */
  chooseGender(gender) {
    if (gender === 'hombre') {
      this.gender = 'hombre'
    } else {
      this.gender = 'mujer'
    }
  }

}
