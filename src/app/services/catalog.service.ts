// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

// Models
import { Result } from '../models/result';

@Injectable()
export class CatalogService {

  /**
   * @param http The injected HttpClient.
   */
  constructor(private http: HttpClient) { }

  /**
   * This function returns a Observable of catalog
   */
  getAllProducts(): Observable<Result> {
    return this.http.get<Result>('assets/api/catalog.json');
  }

}
